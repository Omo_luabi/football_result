# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :football_result,
  ecto_repos: [FootballResult.Repo]

# Configures the endpoint
config :football_result, FootballResultWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "981m1oYnvPwHCBvd6XfKhmM7NKEXEBzl+CQaDA6i2tFqp5M+dP7y6kXPrXnMsF4r",
  render_errors: [view: FootballResultWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: FootballResult.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
