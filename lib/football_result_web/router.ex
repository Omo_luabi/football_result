defmodule FootballResultWeb.Router do
  use FootballResultWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", FootballResultWeb do
    pipe_through :api
  end
end
